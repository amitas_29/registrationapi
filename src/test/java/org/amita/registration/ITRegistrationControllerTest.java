package org.amita.registration;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.annotation.Resource;

import org.amita.registration.model.UserDetails;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;



public class ITRegistrationControllerTest extends RegistrationTests {

    @Resource
    private WebApplicationContext webApplicationContext;
	
    private MockMvc mockMvc;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }
    
	  @Test
	    public void registerValidUser_SuccessOk() throws Exception {
	    	
	    	UserDetails newUser = new UserDetails("amita", "Passw0rd", "1983-10-10", "AS1231251");
	    	
	        mockMvc.perform(post("/register")
	                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
	                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser)))
	        		.andExpect(jsonPath("$.SUCCESS", is("User created successfully")))
	                .andExpect(status().isCreated())
		  			.andExpect(status().is(201));
	    }
	  

	  @Test
	    public void testResponse_ContentType() throws Exception {
	    	
		  UserDetails newUser = new UserDetails("amita", "Passw0rd", "1983-10-11", "AS1231251");
	    	
		  mockMvc.perform(post("/register")
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser)))
        		.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE));
	    }

	  @Test
	  public void whenRequestContentTypeMismatch_thenUnsupportedMediaType() throws Exception {
		  
		  mockMvc.perform(post("/register")
				  	.contentType(MediaType.TEXT_PLAIN_VALUE))
				  	.andExpect(status().isUnsupportedMediaType())
			  		.andExpect(status().is(415));
	  }
	 
	  @Test
	  public void whenHttpRequestMethodNotSupported_thenNotFound()  throws Exception {
		 
		  mockMvc.perform(post("/random"))				  
			        .andExpect(status().isNotFound())
		  			.andExpect(status().is(404));
	  }
	  
	  @Test
	  public void whenHttpRequestMethodMismatch_thenMethodNotAllowed()  throws Exception {
	    	
		  mockMvc.perform(get("/register"))
			        .andExpect(status().isMethodNotAllowed())
		  			.andExpect(status().is(405));
	  }
	  
	  @Test
	    public void registerwithInvaldPassowrd() throws Exception {
	    	
	    	UserDetails newUser = new UserDetails("amita", "Password", "1985-10-15", "AS1231251");
	    	
	        mockMvc.perform(post("/register")
	                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
	                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser)))
	                .andExpect(jsonPath("$.fieldErrors[0].message", is("Password cannot be null and must have atleast 4 character, with atleast 1 upper case and 1 number")))
	                .andExpect(jsonPath("$.status", is("400")))
	                .andExpect(status().isBadRequest())
	                .andExpect(status().is(400));
	               
	    }
	  
    @Test
    public void whenInvalidPassword_thenBadRequest() throws Exception {
    	
    	UserDetails newUser = new UserDetails("amita", "Password", "1985-10-16", "AS1231251");
    	
        mockMvc.perform(post("/register")
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser)))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Password cannot be null and must have atleast 4 character, with atleast 1 upper case and 1 number")))
                .andExpect(jsonPath("$.status", is("400")))
                .andExpect(status().isBadRequest())
                .andExpect(status().is(400));
               
    }
    
    
    @Test
    public void whenInvalidUsername_thenBadRequest() throws Exception {
    	
    	UserDetails newUser = new UserDetails("ami ta", "Passw0rd", "1985-10-17", "AS1231251");
    	
        mockMvc.perform(post("/register")
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser)))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("User Name must be alphanumic without spaces")))
                .andExpect(jsonPath("$.status", is("400")))
                .andExpect(status().isBadRequest())
                .andExpect(status().is(400));
               
    }
    @Test
    public void whenNullUsername_thenBadRequest() throws Exception {
    	
    	UserDetails newUser = new UserDetails("", "Passw0rd", "1985-10-18", "AS1231251");
    	
        mockMvc.perform(post("/register")
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser)))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("User Name cannot be null/empty")))
                .andExpect(jsonPath("$.status", is("400")))
                .andExpect(status().isBadRequest())
                .andExpect(status().is(400));
              
               
    }
    
    @Test
    public void whenNullDob_thenBadRequest() throws Exception {
    	
    	UserDetails newUser = new UserDetails("amita1", "Passw0rd", "", "AS1231251");
    	
        mockMvc.perform(post("/register")
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser)))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("Date of birth cannot be null/empty")))
                .andExpect(jsonPath("$.status", is("400")))
                .andExpect(status().isBadRequest())
                .andExpect(status().is(400));
              
               
    }
  
    
    @Test
    public void whenNullSSN_thenBadRequest() throws Exception {
    	
    	UserDetails newUser = new UserDetails("amita1", "Passw0rd", "1985-10-19", "");
    	
        mockMvc.perform(post("/register")
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser)))
                .andExpect(jsonPath("$.fieldErrors[0].message", is("SSN cannot be null/empty")))
                .andExpect(jsonPath("$.status", is("400")))
                .andExpect(status().isBadRequest())
                .andExpect(status().is(400));
    }
  
  
    @Test
    public void whenDuplicateUser_thenConflict() throws Exception {
    	
    	UserDetails newUser1 = new UserDetails("amita1", "Passw0rd", "1985-10-12", "AS1231251");
    	UserDetails newUser2 = new UserDetails("amita1", "Passw0rd", "1985-10-12", "AS1231251");
    	
        mockMvc.perform(post("/register")
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser1)))
                .andExpect(status().isCreated());
               
        mockMvc.perform(post("/register")
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser2)))
                .andExpect(jsonPath("$.message", is("User Already Exist")))
                .andExpect(jsonPath("$.statusCode", is("CONFLICT")))
                .andExpect(jsonPath("$.status", is("409")))
                .andExpect(status().isConflict())
                .andExpect(status().is(409));

    }
  
    @Test
    public void whenBlackListedUser_thenForbidden() throws Exception {
    	UserDetails newUser = new UserDetails("amita1", "Passw0rd", "1990-10-10", "AS1231231");
                   
        mockMvc.perform(post("/register")
                .contentType(IntegrationTestUtil.APPLICATION_JSON_UTF8)
                .content(IntegrationTestUtil.convertObjectToJsonBytes(newUser)))
                .andExpect(status().isForbidden())
                .andExpect(jsonPath("$.message", is("User cannot be created as this user is in blacklist")))
                .andExpect(jsonPath("$.statusCode", is("FORBIDDEN")))
                .andExpect(jsonPath("$.status", is("403")))
        		.andExpect(status().isForbidden())
                .andExpect(status().is(403));
               
    }


}

