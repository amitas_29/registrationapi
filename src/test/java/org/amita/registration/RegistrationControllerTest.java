package org.amita.registration;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

import org.amita.registration.controller.RegistrationController;
import org.amita.registration.model.UserDetails;
import org.amita.registration.service.ExclusionService;
import org.amita.registration.service.RegistrationService;
import org.amita.registration.util.BlacklistUserException;
import org.amita.registration.util.DataExistException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Validator;



@RunWith(MockitoJUnitRunner.class)
public class RegistrationControllerTest{

	@Mock
	private RegistrationService registrationServiceMock;
	
	@Mock
	private ExclusionService exclusionServiceMock;

	@InjectMocks
	private RegistrationController controller;
	
	@Mock
	private Validator validator;

	@Before
	public void setUp() {
		 MockitoAnnotations.initMocks(this);
	}
	
	
	/**
	 * This test case is for testing a successful user creation
	 * This tests verifies: 
	 * 			1) Number of interactions with dependent classes during this operation
	 * 			2) Success Message 
	 */
	
	@Test
	public void registerNewUserSuccess() throws Exception {
		UserDetails newUserDtls = new UserDetails("amita", "Passw0rd", "1985-10-10", "AS1231251");
		when(exclusionServiceMock.validate(anyString(), anyString())).thenReturn(true);
		when(registrationServiceMock.register(newUserDtls)).thenReturn(true);
		ResponseEntity<String> actual = controller.registerUser(newUserDtls);
		verify(registrationServiceMock, times(1)).register(newUserDtls);
		verify(exclusionServiceMock, times(1)).validate(anyString(), anyString());
		verifyNoMoreInteractions(registrationServiceMock);
		assertEquals("{\"SUCCESS\": \"User created successfully\"}", actual.getBody());
	}
	
	
	/**
	 * This test case is for testing a successful user creation
	 * This tests verifies: 
	 * 			1) Number of interactions with dependent classes during this operation
	 * 			2) Exception type verification 
	 */
	
	
	@Test(expected=BlacklistUserException.class)
	public void registerBlackListedUser() throws Exception {
		UserDetails newUserDtls = new UserDetails("amita", "Passw0rd", "1985-10-10", "AS1231251");
		when(exclusionServiceMock.validate(anyString(), anyString())).thenReturn(false);
		when(registrationServiceMock.register(newUserDtls)).thenReturn(true);
		controller.registerUser(newUserDtls);
		verify(registrationServiceMock, times(0)).register(newUserDtls);
		verify(exclusionServiceMock, times(1)).validate(anyString(), anyString());
		verifyNoMoreInteractions(registrationServiceMock,exclusionServiceMock);
	}
	
	/**
	 * This test case is for testing a successful user creation
	 * This tests verifies: 
	 * 			1) Number of interactions with dependent classes during this operation
	 * 			2) Exception type verification 
	 */
	
	
	@Test(expected=DataExistException.class)
	public void registerDuplicateUser() throws Exception {
		UserDetails newUserDtls = new UserDetails("amita", "Passw0rd", "1985-10-10", "AS1231251");
		when(exclusionServiceMock.validate(anyString(), anyString())).thenReturn(true);
		when(registrationServiceMock.register(newUserDtls)).thenReturn(false);
		controller.registerUser(newUserDtls);
		verify(registrationServiceMock, times(1)).register(newUserDtls);
		verify(exclusionServiceMock, times(1)).validate(anyString(), anyString());
		verifyNoMoreInteractions(registrationServiceMock);
	}
	
	
}








