package org.amita.registration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@EnableWebMvc
@ComponentScan(basePackages = {
        "org.amita.registration.controller",
        "org.amita.registration.service",
        "org.amita.registration.util",
        "org.amita.registration.database"
})
public class RegistrationTests {

	@Test
	public void contextLoads() {
	}

}
