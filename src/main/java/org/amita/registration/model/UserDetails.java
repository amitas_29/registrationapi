package org.amita.registration.model;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * 
 * User Input Details DTO
 * All the validations are added in this class 
 * @author Amita
 *
 */
@JsonInclude
public class UserDetails {
	
	@NotEmpty(message = "User Name cannot be null/empty")
	@Pattern(regexp = "[a-zA-Z0-9]*", message = "User Name must be alphanumic without spaces")
	private String username;
	@Pattern(regexp = "^(?=.*?[A-Z])(?=.*?[0-9]).{4,}", message = "Password cannot be null and must have atleast 4 character, with atleast 1 upper case and 1 number")
	private String password;
	@NotEmpty(message = "Date of birth cannot be null/empty")
	private String dob;
	@NotEmpty(message = "SSN cannot be null/empty")
	private String ssn;
	

	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getSsn() {
		return ssn;
	}
	public void setSsn(String ssn) {
		this.ssn = ssn;
	}
	
	
	/**
	 * overriding hashcode method to use date of birth and ssn number 
	 * if ssn number and dob of 2 users are same then they will be considered same object
	 * 
	 */
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dob == null) ? 0 : dob.hashCode());
		result = prime * result + ((ssn == null) ? 0 : ssn.hashCode());
		return result;
	}
	
	/**
	 * overriding equal method to use date of birth and ssn number for logical comparison of users
	 * if ssn number and dob of 2 users are same then they will be considered equal
	 * 
	 */
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserDetails other = (UserDetails) obj;
		if (dob == null) {
			if (other.dob != null)
				return false;
		} else if (!dob.equals(other.dob))
			return false;
		if (ssn == null) {
			if (other.ssn != null)
				return false;
		} else if (!ssn.equals(other.ssn))
			return false;
		return true;
	}
	
	@Override
	public String toString() {
		return "UserDetails [username=" + username + ", password=" + password + ", dob=" + dob + ", ssn=" + ssn + "]";
	}
	
	
	
	public UserDetails() {
		
	}
	public UserDetails(String username, String password, String dob, String ssn) {
		this.username = username;
		this.password = password;
		this.dob = dob;		
		this.ssn = ssn;
	}
	public UserDetails(String dob, String ssn) {
		this.dob = dob;		
		this.ssn = ssn;
	}
	
	
}
