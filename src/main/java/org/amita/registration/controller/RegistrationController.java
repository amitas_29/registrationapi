package org.amita.registration.controller;

import java.util.Set;

import org.amita.registration.model.UserDetails;
import org.amita.registration.service.ExclusionService;
import org.amita.registration.service.RegistrationService;
import org.amita.registration.util.BlacklistUserException;
import org.amita.registration.util.DataExistException;
import org.amita.registration.util.DataValidationException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Validator;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * This is the controller class for Registartion Api
 *
 */

@RestController
public class RegistrationController {

	final static Logger logger = Logger.getLogger(RegistrationController.class);
	@Autowired
	ExclusionService exclusionService;

	@Autowired
	RegistrationService registrationService;

	@Autowired
	private Validator validator;
	
/**
 * 
 * This method is used for registering a user
 * @param userDtls
 * @return string  - Success message on success registration of user
 * Error message if there are any validation errors 
 * Error Message if user user is blacklisted user or  duplicate user
 * 
 */
	
	@RequestMapping(value = "/register", 
			method = RequestMethod.POST ,
			consumes="application/json")
	
	public ResponseEntity<String> registerUser(@RequestBody UserDetails userDtls){

		this.validate(userDtls, "userDtls");
		
		if (!exclusionService.validate(userDtls.getDob(), userDtls.getSsn())) {
			throw new BlacklistUserException("User cannot be created as this user is in blacklist");
		}
		boolean newUserAdded = registrationService.register(userDtls);
		if (!newUserAdded) {
			throw new DataExistException("User Already Exist");
		} 
		final HttpHeaders httpHeaders= new HttpHeaders();
		httpHeaders.setContentType(MediaType.APPLICATION_JSON_UTF8);
		return new ResponseEntity<String>("{\"SUCCESS\": \"User created successfully\"}", httpHeaders, HttpStatus.CREATED);
		
	}

	
	/**
	 * Method to get list of all users
	 * 
	 * @return Set of User Details
	 */
	
	@RequestMapping("/users")
	public Set<UserDetails> getAllUsers() {
		return registrationService.getAllUsers();
	}
	
	/**
	 * This method is used for validating input parameters
	 * 
	 * @param validated - Object to be validated
	 * @param objectName - Name of Object 
	 */

	private void validate(Object validated, String objectName) {
		logger.debug("Object to be validated : " + validated);

		BeanPropertyBindingResult bindingResult = new BeanPropertyBindingResult(validated, objectName);
		validator.validate(validated, bindingResult);

		if (bindingResult.hasErrors()) {
			throw new DataValidationException("Invalid User Details", bindingResult);
		}

	}
	
	
	
}
