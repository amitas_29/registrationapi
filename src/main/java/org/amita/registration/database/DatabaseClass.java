package org.amita.registration.database;

import java.util.HashSet;
import java.util.Set;

import org.amita.registration.model.UserDetails;
import org.springframework.stereotype.Component;


/**
 * 
 * Redundant class created to act as in memory database
 * @author Amita
 *
 */

@Component
public class DatabaseClass {

	public Set<UserDetails> blackListedUsers = new HashSet<UserDetails>();
	public Set<UserDetails> existingUsers = new HashSet<UserDetails>();

/**
 * This method is for creating list of blacklisted users
 * 
 * @return Set of blacklisted users
 */
	public Set<UserDetails> getAllBlackListedUsers() {
		UserDetails m1 = new UserDetails("1990-10-10", "AS1231231");
		UserDetails m2 = new UserDetails("1980-10-10", "AT3123453");
		blackListedUsers.add(m1);
		blackListedUsers.add(m2);
		return blackListedUsers;
	}
	
	/**
	 * This method is for creating list of existing users
	 * @return
	 * Set of existing users
	 * 
	 */
	public Set<UserDetails> getExistingUsers() {
		return existingUsers;
	}
}
