package org.amita.registration;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.apache.log4j.Logger;
/*
 * Starting point for spring boot application
 * 
 */
@SpringBootApplication
public class RegistrationApplication {

	final static Logger logger = Logger.getLogger(RegistrationApplication.class);

	public static void main(String[] args) {
		
		SpringApplication.run(RegistrationApplication.class, args);
		logger.debug("Starting user registration process");
	}
}
