package org.amita.registration.util;

import org.springframework.validation.Errors;

/**
 * Custom Exception class for handling all type of data validations
 * @author Amita
 *
 */

@SuppressWarnings("serial")
public class DataValidationException extends RuntimeException {
	
	private Errors errors;

	public DataValidationException(String message, Errors errors) {
		super(message);
		this.errors = errors;
	}

	public Errors getErrors() {
		return errors;
	}
}