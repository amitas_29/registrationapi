package org.amita.registration.util;

/**
 * Custom Exception class for handling duplicate user exceptions
 * @author Amita
 *
 */
@SuppressWarnings("serial")
public class DataExistException extends RuntimeException {
	
	public DataExistException(String message) {
		super(message);
	}

	
}