package org.amita.registration.util;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Custom Exception Handler class for handling all types of exceptions while processing register request
 * 
 * Exception Handled are : 
 * 
 * 	@ExceptionHandler({ DataValidationException.class })
	@ResponseStatus(HttpStatus.UNPROCESSABLE_ENTITY)
	
	@ExceptionHandler({ BlacklistUserException.class })
	@ResponseStatus(HttpStatus.FORBIDDEN)
	
	@ExceptionHandler({ DataExistException.class })
	@ResponseStatus(HttpStatus.CONFLICT)
	
 * @author Amita
 */

@ControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

	/**
	 * @param e
	 * @param request
	 * @return
	 */
	@ExceptionHandler({ DataValidationException.class })
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	protected ResponseEntity<Object> handleInvalidRequest(RuntimeException e, WebRequest request) {
		DataValidationException dataValidationEx = (DataValidationException) e;
		List<FieldErrorResource> fieldErrorResources = new ArrayList<>();

		List<FieldError> fieldErrors = dataValidationEx.getErrors().getFieldErrors();
		for (FieldError fieldError : fieldErrors) {
			FieldErrorResource fieldErrorResource = new FieldErrorResource();
			fieldErrorResource.setResource(fieldError.getObjectName());
			fieldErrorResource.setField(fieldError.getField());
			fieldErrorResource.setCode(fieldError.getCode());
			fieldErrorResource.setMessage(fieldError.getDefaultMessage());
			fieldErrorResources.add(fieldErrorResource);
		}

		ErrorResource error = new ErrorResource(HttpStatus.BAD_REQUEST.toString(),
				HttpStatus.BAD_REQUEST.name(), dataValidationEx.getMessage());
		error.setFieldErrors(fieldErrorResources);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		return handleExceptionInternal(e, error, headers, HttpStatus.BAD_REQUEST, request);

	}

	@ExceptionHandler({ BlacklistUserException.class })
	@ResponseStatus(HttpStatus.FORBIDDEN)
	protected ResponseEntity<Object> handleBlackListedUser(RuntimeException e, WebRequest request) {
		BlacklistUserException blacklistUserEx = (BlacklistUserException) e;
		ErrorResource error = new ErrorResource(HttpStatus.FORBIDDEN.toString(), HttpStatus.FORBIDDEN.name(),
				blacklistUserEx.getMessage());

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return handleExceptionInternal(e, error, headers, HttpStatus.FORBIDDEN, request);

	}
	
	
	@ExceptionHandler({ DataExistException.class })
	@ResponseStatus(HttpStatus.CONFLICT)
	protected ResponseEntity<Object> handleDuplicateUser(RuntimeException e, WebRequest request) {
		DataExistException dataExistEx = (DataExistException) e;
		ErrorResource error = new ErrorResource(HttpStatus.CONFLICT.toString(), HttpStatus.CONFLICT.name(),
				dataExistEx.getMessage());

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		return handleExceptionInternal(e, error, headers, HttpStatus.CONFLICT, request);

	}
}
