package org.amita.registration.util;

/**
 * Custom Exception class for handling exceptions for blackListed users
 * @author Amita
 *
 */
@SuppressWarnings("serial")
public class BlacklistUserException extends RuntimeException {
	
	public BlacklistUserException(String message) {
		super(message);
	}

}