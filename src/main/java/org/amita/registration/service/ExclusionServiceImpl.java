package org.amita.registration.service;

import org.amita.registration.database.DatabaseClass;
import org.amita.registration.model.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Service to offer validation of a user against a 'blacklist'. Blacklisted
 * users fail the validation.
 * 
 * @author amita
 *
 */

@Service
public class ExclusionServiceImpl implements ExclusionService {
	@Autowired
	DatabaseClass databasePlaceHolder;

	/**
	 * Validates a user against a black list using his date of birth and social
	 * security number as identifier.
	 *
	 * @param dob
	 *            the user's date of birth in ISO 8601 format
	 * @param ssn
	 *            the user's social security number (United States)
	 * @return true if the user could be validated and is not blacklisted, false
	 *         if the user is blacklisted and therefore failed validation
	 */

	public boolean validate(String dob, String ssn) {
		UserDetails testUser = new UserDetails(dob, ssn);
		return !databasePlaceHolder.getAllBlackListedUsers().contains(testUser);

	}

}