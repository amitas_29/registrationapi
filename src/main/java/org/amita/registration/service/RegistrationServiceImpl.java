package org.amita.registration.service;

import java.util.Set;

import org.amita.registration.database.DatabaseClass;
import org.amita.registration.model.UserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
/**
* Service to register a new user. 
*
*/

@Service
public class RegistrationServiceImpl implements RegistrationService{
	@Autowired
	DatabaseClass databasePlaceHolder;
	
	/**
	 * Method to register a new user
	 *
	 * @param userdetails
	 *            user details
	 * @return true if the user is a new user, 
	 * 		  false if the user is an existing user
	 */

	public boolean register(UserDetails userdetails){
		return databasePlaceHolder.getExistingUsers().add(userdetails);
	}
	
	/**
	 * Method to get list of all users
	 *
	 * 
	 * @return Set of User Details
	 */

	public Set<UserDetails> getAllUsers(){
		return databasePlaceHolder.getExistingUsers();
	}
	
	
	
}
